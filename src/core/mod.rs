/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

mod engine;
mod environment;
mod game_framework;
mod platform;
mod scene;
mod service;
mod state;
mod task;
mod uobject;
mod usystem;

// Re-Export all public members, since public members are re-exported by the modules themselves
pub use environment::*;
pub use environment::*;
pub use game_framework::*;
pub use platform::*;
pub use scene::*;
pub use service::*;
pub use state::*;
pub use task::*;
pub use uobject::*;
pub use usystem::*;

use std::any::Any;
use std::any::TypeId;
use std::sync::Arc;

/// Defines an interface for a `Subject` to update this object when it changes state
pub trait Observer {
    /// Called when a `Subject` sends an event to this `Observer`
    ///
    /// It is expected that the `Observer` knows the concrete type of the object it has
    /// subscribed to.
    fn update(&self, subject: *const dyn Any, event: String);
}

/// Interface which allows objects with `Observer` to get notified when the state of the
/// object that extends `Subject` changes
pub trait Subject {
    /// Attach an `Observer` to receive updates when the `Subject`'s state changes
    ///
    /// State changes are not sent immediately, they are all sent at the same time,
    /// after each Service has finished the jobs they need to do.
    fn attach(&mut self, observer: Box<dyn Observer>);
    /// Detach an `Observer` from the Subject
    fn detach(&self, observer: Box<dyn Observer>);
    /// Notify all interested `Observer`s of a state change
    ///
    /// Will notify the observers with a pointer to `self` as an `Any`, downcasting it
    /// to the concrete type is the responsibility of the observer and it is assumed they
    /// are aware of the concrete type they are listening to.
    fn notify(&self, event: String);
}

/// Represents an interface that doesn't exist
pub struct Nothing {
    // There's nothing here
}
