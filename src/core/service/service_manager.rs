/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use super::super::USystem;
use std::any::Any;

/// Manager for storing and retrieving service interfaces
pub struct ServiceManager {
    registration_finished: bool,
    services: Vec<Box<dyn Any>>,
}

impl ServiceManager {
    /// Start a `ServiceManager`
    ///
    /// Although this class does not validate that there is only one running instance
    /// of the `ServiceManager`, there should never be more than one instance created
    /// at a time.
    pub(crate) fn start() -> ServiceManager {
        ServiceManager {
            registration_finished: false,
            services: vec![],
        }
    }

    /// Indicates that new `Service` interfaces should not be added anymore
    pub(crate) fn finished_registration(&mut self) {
        self.registration_finished = true;
    }

    /// Register a service interface to the `ServiceManager`
    ///
    /// There is no validation that this is a valid interface,
    /// if it is not, it will never be returned by `Self::get_service`
    pub fn register_service(&mut self, service: Box<dyn Any>) -> Result<(), &str> {
        match self.registration_finished {
            true => return Err("Service registration period has passed"),
            false => {
                // Push the service into the storage
                self.services.push(service);
                return Ok(());
            }
        }
    }

    /// Try to get a service interface given the service's `USystem` type
    pub fn get_service<T: USystem + 'static>(&mut self) -> Option<&T::SystemServiceInterface> {
        for service in &self.services {
            if service.is::<T>() {
                return Some(unsafe {
                    // Safety: We just checked that this is the correct type
                    // additionally this should not panic because we're converting
                    // from a type that has the lifetime of this object
                    (service as *const dyn Any as *const T::SystemServiceInterface)
                        .as_ref()
                        .unwrap()
                });
            }
        }

        None
    }
}
