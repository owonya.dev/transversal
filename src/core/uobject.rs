/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::any::Any;

use super::{Observer, Subject, USystem};

/// The universal trait for objects
pub trait UObject: Observer + Subject {
    /// Try to get an extension from the UObject, returns a reference to the `Object` type of the given extension
    ///
    /// # Safety
    /// The function uses unsafe code, but everything is validated and should not result in any UB maybe.
    fn get_extension<T: USystem + 'static>(&mut self) -> Option<&T::SystemObject>;
}

// Code which merely is an example of how to derive `UObject`

/// Example code for derivation
struct UObjectMembers {
    extension_objects: Vec<*const dyn Any>,
}

impl UObject for UObjectMembers {
    fn get_extension<T: USystem + 'static>(&mut self) -> Option<&T::SystemObject> {
        for object in &self.extension_objects {
            if let Some(object_ref) = unsafe { object.as_ref() } {
                if object_ref.is::<T::SystemObject>() {
                    unsafe {
                        // Safety: We just checked if this type is the type we're casting it to
                        // so it is safe to do so here.
                        let owo = *object as *const T::SystemObject;
                        return Some(owo.as_ref().unwrap());
                    };
                }
            } else {
                // Drop the extension out of the UObject
                // TODO!
            }
        }

        None
    }
}

impl Subject for UObjectMembers {
    fn attach(&mut self, _observer: Box<dyn Observer>) {
        unimplemented!()
    }
    fn detach(&self, _observer: Box<dyn Observer>) {
        unimplemented!()
    }
    fn notify(&self, _event: String) {
        unimplemented!()
    }
}

impl Observer for UObjectMembers {
    fn update(&self, _subject: *const dyn Any, _event: String) {
        unimplemented!()
    }
}
