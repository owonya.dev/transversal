/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::core::Subject;
use std::any::Any;
use std::ptr::null;

/// Manages object state propagation
///
/// This is an internal engine service and should not be used from Service or User code.
///
/// Additionally, it should only be created once.
pub struct EngineStateManager {
    state_changes: Vec<(*const dyn Subject, String)>,
}

impl EngineStateManager {
    /// Push a state change into the queue
    ///
    /// Will throw out any changes where the pointer isn't valid and panic. This
    /// is likely to be changed in the future to just an error in some console.
    ///
    /// # Panics
    /// Will panic if the pointer passed is null
    ///
    /// # Safety
    /// Panics if the `*const dyn Subject` is `null`, preventing UB
    pub fn changed_state(&mut self, object: *const dyn Subject, event: String) {
        if object.is_null() {
            // Something went wrong somewhere oh god ;w;
            panic!("Pointer passed to the state manager is null");
        }
        self.state_changes.push((object, event));
    }

    /// Distribute the stored change notifications among the `Observer`s
    ///
    /// # Safety
    /// Its probably safe? IDK it checks for null and stuff?
    pub fn distribute(&mut self) {
        // TODO:
        // In the future it is best if there are a few more mechanisms to handle
        // change distribution, such as looking at two events that are the "same"
        // and deciding which one is higher priority based on priority set by the
        // event sender, as well as time. Right now the last event is always the
        // accepted one, which may cause issues in the future.

        for change in &self.state_changes {
            let (subject, event) = change;
            if !subject.is_null() {
                let as_ref = unsafe { subject.as_ref() }.unwrap();

                as_ref.notify(event.clone());
                // TODO: May break things
                drop(subject);
            } else {
                drop(subject);
            }
        }

        self.state_changes.clear();
    }
}
