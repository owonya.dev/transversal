/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use super::{Observer, Subject};

/// Interface used to define standard methods for systems
///
/// Additionally, provides the System's associated types
pub trait USystem {
    /// Type used for the Object representation of `UObjects` in this System
    type SystemObject: Observer + Subject;
    /// Type used for this System's Scene
    type SystemScene;
    /// Type used if this System provides an interface as a Service
    ///
    /// If the Service provides no interface, this can evaluate to `Never`
    type SystemServiceInterface;
}
