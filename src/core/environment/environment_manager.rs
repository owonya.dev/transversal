use std::collections::HashMap;

/// Handles data about the currently running engine and the current scene
pub struct EnvironmentManager {
    // Probably should hold:
    // Named parameters for the current scene
    // Keybind stuff
    // Settings that should persist across scene changes
    // Loading new scenes
    //   - Load systems that the scene requires using the platform manager
    //   - Create a new UScene and call each `System` to make their own scenes and objects
    /// Holds the state of the current environment
    state: EnvironmentState,
}

impl EnvironmentManager {
    /// Returns the value of a named value if it exists
    ///
    /// Named values are passed to the next scene when the engine is given the command to load it.
    pub fn get_named_value(&self, name: String) -> Option<String> {
        if let Some(value) = self.state.named_parameters.get(&name) {
            Some(value.clone())
        } else {
            None
        }
    }

    /// Returns the value assigned to the passed key
    ///
    /// The values in the settings table are persistent across scenes and restarts.
    pub fn get_settings_key(&self, key: String) -> Option<String> {
        if let Some(value) = self.state.named_parameters.get(&key) {
            Some(value.clone())
        } else {
            None
        }
    }
}

/// Holds data about the current environment
pub struct EnvironmentState {
    /// Holds named parameters given when scene was loaded
    pub named_parameters: HashMap<String, String>,
    /// Holds value pairs which are saved and loaded
    ///
    /// Don't use this to store stuff that isn't meant as configuration.
    pub settings: HashMap<String, String>,
}
