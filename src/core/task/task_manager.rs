/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::{Arc, Mutex};
use std::thread;

/// Distributes tasks to the Engine's thread pool
///
/// Not to be confused with Windows's Task Manager ;P
pub struct TaskManager {
    /// Stores the current amount of running tasks
    ///
    /// This could be stored as an AtomicUsize, but it is not.
    current_tasks: Arc<Mutex<usize>>,
}

impl TaskManager {
    /// Run a task on the engine's thread pool
    pub fn run_task<F>(&self, task: F)
    where
        F: Fn() + Send + 'static,
    {
        // Update the counter
        *self.current_tasks.lock().unwrap() += 1;

        let clone = self.current_tasks.clone();
        thread::spawn(move || {
            // Run the task
            task();
            // Update the counter
            *clone.lock().unwrap() -= 1;
        });
    }
}
