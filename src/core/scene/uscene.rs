/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use super::super::{Observer, Subject};

use std::any::Any;

/// A universal scene
///
/// Wraps functionality of `Scene`s
pub struct UScene {
    scenes: Vec<Box<dyn Any>>,
}
