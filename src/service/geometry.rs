/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::any::Any;
use std::ptr::eq;

use crate::core::{EngineStateManager, Nothing, Observer, Subject, USystem};

pub struct System {}

impl USystem for System {
    type SystemObject = Object;
    type SystemScene = Scene;
    type SystemServiceInterface = Nothing;
}

pub struct Scene {}

pub struct Object {
    /// Holds a reference to the state manager (used to propagate state changes)
    state_manager: EngineStateManager,
    /// `Observer`s currently observing this `Object`'s state
    observers: Vec<Box<dyn Observer>>,
    /// UObject that this Object is bound to

    /// Holds the position of the object
    position: (),
    /// Holds the rotation of the object
    rotation: (),
    /// Holds the mesh used to render the object
    mesh: (),
    /// Holds the scale of the object
    scale: (),
}

impl Object {
    /// Returns the position of the `Object`
    pub fn get_position(&self) -> () {
        self.position
    }

    /// Returns the rotation of the `Object`
    pub fn get_rotation(&self) -> () {
        self.rotation
    }

    /// Returns the mesh of the `Object`
    pub fn get_mesh(&self) -> () {
        self.mesh
    }

    /// Returns the scale of the `Object`
    pub fn get_scale(&self) -> () {
        self.scale
    }

    /// Internal helper function to decrease repetitiveness in setter functions
    ///
    /// Wraps sending a change event to the state manager
    fn changed_event(&mut self, event: &str) {
        self.state_manager
            .changed_state(self as *const dyn Subject, event.to_owned());
    }

    /// Sets the position of the Object
    ///
    /// Additionally, queues a state change event in the state manager
    pub fn set_position(&mut self, new_position: ()) {
        // Send an event to the state manager
        self.changed_event("position");
        // Update our version of the position
        self.position = new_position;
    }

    /// Sets the position of the Object
    ///
    /// Additionally, queues a state change event in the state manager
    pub fn set_rotation(&mut self, new_rotation: ()) {
        self.changed_event("rotation");
        self.rotation = new_rotation;
    }

    /// Sets the position of the Object
    ///
    /// Additionally, queues a state change event in the state manager
    pub fn set_mesh(&mut self, new_mesh: ()) {
        self.changed_event("mesh");
        self.mesh = new_mesh;
    }

    /// Sets the position of the Object
    ///
    /// Additionally, queues a state change event in the state manager
    pub fn set_scale(&mut self, new_scale: ()) {
        self.changed_event("scale");
        self.scale = new_scale;
    }
}

impl Subject for Object {
    /// Attach an `Observer` to receive updates when the `Subject`'s state changes
    fn attach(&mut self, observer: Box<dyn Observer>) {
        self.observers.push(observer);
    }

    /// Detach an `Observer` from the Subject
    fn detach(&self, observer: Box<dyn Observer>) {
        for stored_observer in &self.observers {
            // This might work?
            if eq(&observer, stored_observer) {
                println!("De-registering observer");
            }
        }
    }

    /// Notify all interested `Observer`s of a state change
    fn notify(&self, event: String) {
        for observer in &self.observers {
            observer.update(self as *const dyn Any, event.clone());
        }
    }
}

impl Observer for Object {
    fn update(&self, _subject: *const dyn Any, _event: String) {
        unimplemented!()
    }
}
