/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/// Handles physical rendered representations of `UObjects`
///
/// Stores data about a `UObject` that defines its presence in a 3D world
pub mod geometry;
mod graphics;
